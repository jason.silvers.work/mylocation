package silverarrow.roadrunner;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

@SpringBootTest
class RoadRunnerTests {

	@Test
	void contextLoads() {
		MoodAnalyzer moodAnalyzer = new MoodAnalyzer();

		String mood = moodAnalyzer.analyzeMood("sad");

		Assert.assertEquals(mood, "sad");

		Assert.assertThat(moodAnalyzer.analyzeMood("sad"), is(equalTo("sad")));

	}

}
