package silverarrow.roadrunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoadRunner {

	public static void main(String[] args) {
		SpringApplication.run(RoadRunner.class, args);
	}

}
