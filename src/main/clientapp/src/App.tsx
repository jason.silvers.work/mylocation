import React from 'react';
import './App.css';
import Test from './Test'
import { Button } from '@material-ui/core';

function App(): JSX.Element {
    return (
        <div className="App">
            <Test message={'You are super awesome'}/>
            <Button variant="outlined" color="primary">Hello World</Button>
        </div>
    );
}

export default App;
