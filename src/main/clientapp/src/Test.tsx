import React, { FunctionComponent } from 'react';

interface OwnProps {message: string}

type Props = OwnProps;

const Test: FunctionComponent<Props> = (props) => {

  return (
      <div>
          <p>{props.message}</p>
      </div>
  );
};

export default Test;
